TOPTARGETS := check test build clean
SUBDIRS = $(shell find . -mindepth 2 -maxdepth 2 -name Makefile -exec dirname \{\} \;)

.PHONY: pre-commit-update $(TOPTARGETS) $(SUBDIRS)

all: .git/hook/pre-commit $(SUBDIRS)

.git/hook/pre-commit: .pre-commit-config.yaml
	pre-commit install

pre-commit-update:
	pre-commit autoupdate
	pre-commit gc

$(TOPTARGETS): $(SUBDIRS)

$(SUBDIRS):
	$(MAKE) -C $@ $(MAKECMDGOALS)

check-dependency:
	$(MAKE) -C spark8s $(MAKECMDGOALS)
