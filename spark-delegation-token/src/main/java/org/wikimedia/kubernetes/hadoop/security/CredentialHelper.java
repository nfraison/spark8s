/*
 * Copyright (C) 2023  Wikimedia Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.01
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.wikimedia.kubernetes.hadoop.security;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hdfs.HdfsConfiguration;
import org.apache.hadoop.hive.conf.HiveConf;
import org.apache.hadoop.hive.ql.metadata.Hive;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.shims.Utils;
import org.apache.hadoop.security.Credentials;
import org.apache.hadoop.security.UserGroupInformation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CredentialHelper {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private static final String HIVE_DELEGATION_TOKEN_SERVICE_NAME = "DelegationTokenForHiveMetaStoreServer";
    private final Configuration conf = new HdfsConfiguration();
    private final Path tokenFilePath;
    private final UserGroupInformation user;

    public CredentialHelper(String tokenFile) throws IOException {
        java.nio.file.Path tokenFilePath = Paths.get(tokenFile);
        if (Files.exists(tokenFilePath) && !Files.isRegularFile(tokenFilePath)) {
            throw new IOException(String.format("Token file %s already exist but is not a file", tokenFilePath.toUri()));
        }
        this.tokenFilePath = new Path(tokenFilePath.toUri());
        this.user = UserGroupInformation.getCurrentUser();
    }

    private void addHdfsCredentials() throws IOException {
        logger.info("Add hdfs token to current user {}", this.user.getShortUserName());

        Credentials cred = new Credentials();

        FileSystem fs = FileSystem.get(this.conf);
        fs.addDelegationTokens(this.user.getUserName(), cred);
        fs.close();

        this.user.addCredentials(cred);
    }

    private void addHiveCredentials() throws IOException, HiveException {
        logger.info("Add hive token to current user {}", this.user.getShortUserName());

        HiveConf hiveConf = new HiveConf();
        org.apache.hadoop.hive.ql.metadata.Hive hive = org.apache.hadoop.hive.ql.metadata.Hive.get(hiveConf);

        String hiveToken = hive.getDelegationToken(this.user.getShortUserName(), this.user.getShortUserName());
        Utils.setTokenStr(this.user, hiveToken, HIVE_DELEGATION_TOKEN_SERVICE_NAME);

        Hive.closeCurrent();
    }

    public void addHadoopCredentials() throws IOException, HiveException {
        this.addHdfsCredentials();
        this.addHiveCredentials();
    }

    public void writeTokenStorageFile() throws IOException {
        logger.info("Write Hadoop Delegation Tokens to credential file {}", this.tokenFilePath.toUri());
        this.user.getCredentials().writeTokenStorageFile(this.tokenFilePath, this.conf);
    }
}
