/*
 * Copyright (C) 2023  Wikimedia Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.01
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.wikimedia.kubernetes.hadoop.security;

import java.io.IOException;
import java.security.PrivilegedExceptionAction;

import org.apache.hadoop.security.UserGroupInformation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class Main {
    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) throws IOException, InterruptedException {
        if (args.length == 0) {
            logger.error("Proper Usage is: java -cp spark-delegation-token.jar:<hadoop classpath>:<hive classpath> " +
                    "org.wikimedia.kubernetes.hadoop.security.Main token_file_path");
            System.exit(1);
        }

        CredentialHelper credsHelp = new CredentialHelper(args[0]);

        UserGroupInformation.getCurrentUser().doAs(
                (PrivilegedExceptionAction<Object>) () -> {
                    credsHelp.addHadoopCredentials();

                    credsHelp.writeTokenStorageFile();
                    return null;
                });
    }
}
