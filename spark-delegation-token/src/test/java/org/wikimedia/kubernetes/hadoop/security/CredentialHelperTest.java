/*
 * Copyright (C) 2023  Wikimedia Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.01
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.wikimedia.kubernetes.hadoop.security;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CredentialHelperTest {
    CredentialHelper credsHelper;
    Path tempTokenFile;

    @BeforeEach
    public void setUp() throws IOException {
        String tmpdir = Files.createTempDirectory("hdt").toFile().getAbsolutePath();
        System.setProperty("user.dir", tmpdir);
        tempTokenFile = Files.createTempFile("", ".tmp");
        this.credsHelper = new CredentialHelper(tempTokenFile.toString());
    }

    @Test
    public void throw_exception_token_exist_but_not_a_file() throws IOException {
        String tmpdir = Files.createTempDirectory("hdt").toFile().getAbsolutePath();
        assertThrows(IOException.class, () -> new CredentialHelper(tmpdir));
    }

    @Test
    public void token_file_is_well_created() throws IOException {
        this.credsHelper.writeTokenStorageFile();
        assertTrue(tempTokenFile.toFile().isFile());
    }

}
