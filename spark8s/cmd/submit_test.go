/*
Copyright © 2023 Wikimedia Foundation

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"os"
	"testing"
)

func TestGetRootTempPath(t *testing.T) {
	home := "/home/sparkuser"
	os.Setenv("HOME", home)
	assert.Equal(t, getRootTempPath(), fmt.Sprintf("%s/.spark8s", home))
}

func TestCreateTemporaryFolder(t *testing.T) {
	rootTempPath, err := os.MkdirTemp(os.TempDir(), "spark_test")
	if err != nil {
		t.Fatalf("Failed to create root temporary folder %v", err)
	}
	defer os.RemoveAll(rootTempPath)

	tempPath, err := createTemporaryFolder(rootTempPath)
	if err != nil {
		t.Fatalf("Failed to create root temporary folder %v", err)
	}

	stat, err := os.Stat(tempPath)
	if err != nil {
		t.Fatalf("Failed to get stats for temporary folder %s", tempPath)
	}

	if stat.Mode().Perm() != 0o700 {
		t.Fatalf("Temporary folder %s does note have appropriate 700 permissions", tempPath)
	}
}
