/*
Copyright © 2023 Wikimedia Foundation

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package utils

import (
	"bytes"
	"encoding/base64"
	"fmt"
	"github.com/google/uuid"
	log "github.com/sirupsen/logrus"
	"k8s.io/apimachinery/pkg/util/yaml"
	"os"
	"os/exec"
	"reflect"
	"text/template"
)

func GetUuid() string {
	id := uuid.New()
	return id.String()
}

func GetNewSparkApplicationName(uuid string) string {
	return fmt.Sprintf("%s-%s-%s", RootSparkAppName, os.Getenv("USER"), uuid)
}

func GetBase64EncodedFileData(filePath string) (string, error) {
	creds, err := os.ReadFile(filePath) // #nosec G304
	if err != nil {
		return "", fmt.Errorf("failed to read file %s: %v", filePath, err)
	}
	credsBase64Enc := base64.StdEncoding.EncodeToString(creds)
	return credsBase64Enc, nil
}

func Contains(item string, list []string) bool {
	for _, b := range list {
		if b == item {
			return true
		}
	}
	return false
}

func CreateObjectFromTemplate(sparkApp *SparkApp, templateToRender string, into interface{}) error {
	log.WithFields(log.Fields{"template": templateToRender}).Debug("Render template")
	tmpl := template.New("SparkApplication")
	parsedTemplate, err := tmpl.Parse(templateToRender)
	if err != nil {
		return fmt.Errorf("failed to parse template: %v", err)
	}
	var renderedTemplate bytes.Buffer
	if err = parsedTemplate.Execute(&renderedTemplate, sparkApp); err != nil {
		return fmt.Errorf("failed to render template: %v", err)
	}

	decoder := yaml.NewYAMLOrJSONDecoder(&renderedTemplate, 1024)
	err = decoder.Decode(into)
	if err != nil {
		return fmt.Errorf("failed to create object type %v from template: %v", reflect.TypeOf(into), err)
	}
	return nil
}

func RunCommand(name string, arg ...string) (string, error) {
	cmd := exec.Command(name, arg...) // #nosec G204
	stdout, err := cmd.Output()
	if err != nil {
		errorMsg := err.Error()
		if e, ok := err.(*exec.ExitError); ok {
			errorMsg = string(e.Stderr)
		}
		return "", fmt.Errorf("failed to execute command (%v):\n%s", cmd, errorMsg)
	}
	log.WithFields(log.Fields{"cmd": cmd}).Debugf("Standard output: %s", string(stdout))
	return string(stdout), nil
}
